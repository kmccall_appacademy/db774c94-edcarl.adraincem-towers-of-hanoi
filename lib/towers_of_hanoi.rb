# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.
require 'byebug'

class TowersOfHanoi
  VALID_INPUTS = ["0", "1", "2"].freeze

  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def play
    puts "Welcome! Let's play."
    puts render
    until won?
      puts "Which pile to select disc from?"
      input_from_tower = ask_move
      puts "Which pile to move disc to?"
      input_to_tower = ask_move
      if valid_move?(input_from_tower, input_to_tower)
        move(input_from_tower, input_to_tower)
      end
      puts render
    end
    puts "***** You win! *****"
  end

  def ask_move
    input = gets.chomp
    until VALID_INPUTS.include?(input)
      puts "Select only from: 0, 1, or 2"
      input = gets.chomp
    end
    input.to_i
  end

  def render
    top = towers.map { |disc| disc.length >= 3 ? "#{disc.last}": "*"}
    mid = towers.map { |disc| disc.length >= 2 ? "#{disc[1]}": "*"}
    low = towers.map { |disc| disc.length >= 1 ? "#{disc.first}": "*"}
    "\n#{top.join('  ')}\n#{mid.join('  ')}\n#{low.join('  ')}\n\n"
  end

  def won?
    towers[2] == [3, 2, 1] || towers[1] == [3, 2, 1]
  end

  def valid_move?(from_tower, to_tower)
    from = towers[from_tower]
    to = towers[to_tower]
    return false if from.empty?
    return false if !to.empty? && from.last > to.last
    true
  end

  def move(from_tower, to_tower)
    disc = towers[from_tower].pop
    towers[to_tower].push(disc)
  end

end

if __FILE__ == $PROGRAM_NAME
  game = TowersOfHanoi.new
  game.play
end
